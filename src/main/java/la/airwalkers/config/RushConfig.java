package la.airwalkers.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @author liupf
 * 公用部分放于父类RushConfig中
 * 子环境各自定义的常量放于子类中
 */
@Configuration
@PropertySource("classpath:rush.properties")
public abstract class RushConfig {
    @Value("${rush.regions}")
    private String[] rushRegions;
    @Value("${rush.xpath.receiving}")
    private String rushXpathReceiving;
    @Value("${login.userpwd}")
    private String loginUserpwd;
    @Value("${login.xpath.username}")
    private String loginXpathUsername;
    @Value("${login.xpath.userpwd}")
    private String loginXpathUserpwd;
    @Value("${login.xpath.submit}")
    private String loginXpathSubmit;
    @Value("${login.url}")
    private String loginUrl;

    public abstract String getLoginUsername();

    public abstract String[] getRushTitleIncludes();

    public abstract String[] getRushTitleExcludes();

    public abstract String getRushUrlDelegate();

    public abstract String getRushPricePattern();

    public String[] getRushRegions() {
        return rushRegions;
    }

    public String getRushXpathReceiving() {
        return rushXpathReceiving;
    }

    public String getLoginUserpwd() {
        return loginUserpwd;
    }

    public String getLoginXpathUsername() {
        return loginXpathUsername;
    }

    public String getLoginXpathUserpwd() {
        return loginXpathUserpwd;
    }

    public String getLoginUrl() {
        return loginUrl;
    }

    public String getLoginXpathSubmit() {
        return loginXpathSubmit;
    }

    @Profile("rent")
    @Component
    public static class RentConfig extends RushConfig {
        @Value("${rush.title.includes.rent}")
        private String[] rushTitleIncludes;

        @Value("${rush.title.excludes.rent}")
        private String[] rushTitleExcludes;

        @Value("${rush.url.delegate.rent}")
        private String rushUrlDelegate;

        @Value("${rush.price.pattern.rent}")
        private String rushPricePattern;

        @Value("${login.username.rent}")
        private String loginUsername;

        @Value("${login.userpwd.rent}")
        private String loginUserpwd;

        @Override
        public String[] getRushTitleIncludes() {
            return rushTitleIncludes;
        }

        @Override
        public String[] getRushTitleExcludes() {
            return rushTitleExcludes;
        }

        @Override
        public String getRushUrlDelegate() {
            return rushUrlDelegate;
        }

        @Override
        public String getRushPricePattern() {
            return rushPricePattern;
        }

        @Override
        public String getLoginUsername() {
            return loginUsername;
        }

        @Override
        public String getLoginUserpwd() {
            return loginUserpwd;
        }
    }

    @Profile("sale")
    @Component
    public static class SaleConfig extends RushConfig {
        @Value("${rush.title.includes.sale}")
        private String[] rushTitleIncludes;

        @Value("${rush.title.excludes.sale}")
        private String[] rushTitleExcludes;

        @Value("${rush.url.delegate.sale}")
        private String rushUrlDelegate;

        @Value("${rush.price.pattern.sale}")
        private String rushPricePattern;

        @Value("${login.username.sale}")
        private String loginUsername;

        @Value("${login.userpwd.sale}")
        private String loginUserpwd;

        @Override
        public String[] getRushTitleIncludes() {
            return rushTitleIncludes;
        }

        @Override
        public String[] getRushTitleExcludes() {
            return rushTitleExcludes;
        }

        @Override
        public String getRushUrlDelegate() {
            return rushUrlDelegate;
        }

        @Override
        public String getRushPricePattern() {
            return rushPricePattern;
        }

        @Override
        public String getLoginUsername() {
            return loginUsername;
        }

        @Override
        public String getLoginUserpwd() {
            return loginUserpwd;
        }
    }
}
