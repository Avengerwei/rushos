package la.airwalkers.config;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import sun.rmi.runtime.Log;

import java.util.concurrent.TimeUnit;

@Configuration
public class DriverConfig {
    private final AppConfig appConfig;

    public DriverConfig(AppConfig appConfig) {
        this.appConfig = appConfig;
    }

    @Bean
    @Qualifier("baseDriver")
    public WebDriver webDriver() {
        WebDriver driver = getDriver();
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);
        return driver;
    }

    private WebDriver getDriver() {
        switch (appConfig.getDriverName()) {
            default:
            case "chrome":
                return chrome(appConfig.getDriverPath());
            case "phantomjs":
                return phantomjs(appConfig.getDriverPath());
        }
    }

    private WebDriver chrome(String path) {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("disable-infobars");
        System.setProperty("webdriver.chrome.driver", path);
        return new ChromeDriver(options);
    }

    private WebDriver phantomjs(String path) {
        System.setProperty("phantomjs.binary.path", path);
        return new PhantomJSDriver();
    }

}
