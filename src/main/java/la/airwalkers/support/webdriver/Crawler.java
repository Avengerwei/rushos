package la.airwalkers.support.webdriver;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * @author liupf
 * 爬虫类
 * 实现基本的网页操作
 */
@Component
public class Crawler implements WebDriver {
    private final WebDriver driver;
    private final Logger logger;

    public Crawler(@Qualifier("baseDriver") WebDriver driver
            , Logger logger) {
        this.driver = driver;
        this.logger = logger;
    }

    @Override
    public void get(String url) {
        try {
            this.driver.get(url);
        } catch (Exception e) {
            this.logger.error("[get]:", e);
        }
    }

    @Override
    public List<WebElement> findElements(By by) {
        try {
            return this.driver.findElements(by);
        } catch (Exception e) {
            this.logger.info("[findElements]:" + by + "\n"
                    + this.driver.getTitle() + "\n"
                    + this.driver.getPageSource());
            return Collections.emptyList();
        }
    }

    @Override
    public WebElement findElement(By by) {
        try {
            return this.driver.findElement(by);
        } catch (Exception e) {
            this.logger.info("[findElement]:" + by + "\n"
                    + this.driver.getTitle() + "\n"
                    + this.driver.getPageSource());
            return null;
        }
    }

    @Override
    @PreDestroy
    public void quit() {
        this.driver.quit();
        this.logger.info("[quit]");
    }

    @Override
    public String getCurrentUrl() {
        return this.driver.getCurrentUrl();
    }

    @Override
    public String getTitle() {
        return this.driver.getTitle();
    }

    @Override
    public String getPageSource() {
        return this.driver.getPageSource();
    }

    @Override
    public void close() {
        this.driver.close();
    }

    @Override
    public Set<String> getWindowHandles() {
        return this.driver.getWindowHandles();
    }

    @Override
    public String getWindowHandle() {
        return this.driver.getWindowHandle();
    }

    @Override
    public TargetLocator switchTo() {
        return this.driver.switchTo();
    }

    @Override
    public Navigation navigate() {
        return this.driver.navigate();
    }

    @Override
    public Options manage() {
        return this.driver.manage();
    }

    /**
     * 点击后处理产生的Alert
     *
     * @param element
     */
    public void clickAlert(WebElement element) {
        if (PhantomJSDriver.class.isInstance(this.driver)) {
            alert();
            click(element);
        } else {
            click(element);
            alert();
        }
    }

    private void alert() {
        try {
            if (PhantomJSDriver.class.isInstance(this.driver)) {
                ((JavascriptExecutor) this.driver).executeScript("window.confirm = function(msg){return true;}");
            } else {
                this.driver.switchTo().alert().accept();
            }
        } catch (Exception e) {
            this.logger.error("[alert]", e);
        }
    }

    /**
     * 点击
     *
     * @param element
     */
    public void click(WebElement element) {
        Optional.ofNullable(element)
                .ifPresent(WebElement::click);
    }
}
