package la.airwalkers.support.webdriver;

import la.airwalkers.rush.Task;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class TaskService {
    private final Task task;

    public TaskService(Task task) {
        this.task = task;
    }

    @Scheduled(fixedRate = 1000)
    public void execute() {
        task.run();
    }
}
