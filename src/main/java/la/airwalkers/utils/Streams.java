package la.airwalkers.utils;

import java.util.*;
import java.util.stream.Stream;

/**
 * @author liupf
 */
public class Streams {
    private Streams() {
        throw new IllegalStateException("uility class");
    }

    public static <T> Stream<T> of(Collection<T> collection) {
        return Optional.ofNullable(collection)
                .map(Collection::stream)
                .orElseGet(Stream::empty);
    }
}
