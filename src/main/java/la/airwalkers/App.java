package la.airwalkers;

import la.airwalkers.config.AppConfig;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;


/**
 * @author liupf
 */
public class App {
    private static Logger LOGGER = Logger.getLogger(App.class);

    public static void main(String[] args) {
        AbstractApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        context.registerShutdownHook();
        LOGGER.info("app.version = " + context.getBean(AppConfig.class).getVersion());
    }
}
