package la.airwalkers.rush;

import la.airwalkers.config.RushConfig;
import la.airwalkers.entity.RecordDTO;
import la.airwalkers.support.webdriver.Crawler;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

/**
 * @author liupf
 */
@Profile("sale")
@Component
public class SaleRusher extends Rusher {

    protected SaleRusher(Crawler crawler
            , Logger logger
            , RushConfig rushConfig) {
        super(crawler, logger, rushConfig);
    }

    @Override
    boolean filter(RecordDTO record) {
        if (record.getArea() <= 40 || record.getArea() >= 350) {
            return false;
        }

        if (!StringUtils.containsAny(record.getRegion(), rushConfig.getRushRegions())) {
            return false;
        }

        if (StringUtils.containsAny(record.getTitle(), rushConfig.getRushTitleExcludes())) {
            return false;
        }

        return true;
    }
}
