package la.airwalkers.rush;

import la.airwalkers.config.RushConfig;
import la.airwalkers.entity.LoginException;
import la.airwalkers.entity.RecordDTO;
import la.airwalkers.support.webdriver.Crawler;
import la.airwalkers.utils.Streams;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.stereotype.Component;
import org.springframework.util.ConcurrentReferenceHashMap;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author liupf
 */
public abstract class Rusher implements Task{
    private final Crawler crawler;
    private final Logger logger;
    protected final RushConfig rushConfig;
    private final Pattern pattern;

    protected Rusher(Crawler crawler, Logger logger, RushConfig rushConfig) {
        this.crawler = crawler;
        this.logger = logger;
        this.rushConfig = rushConfig;
        this.pattern = Pattern.compile(rushConfig.getRushPricePattern());
    }

    @Override
    public void run() {
        try {
            fetch();
        } catch (LoginException e) {
            sleep(2000);
            crawler.manage().deleteAllCookies();
            login();
        }
    }

    private void login() {
        logger.info("login");

        crawler.get(rushConfig.getLoginUrl());

        Optional.of(By.xpath(rushConfig.getLoginXpathUsername()))
                .map(crawler::findElement)
                .ifPresent(elem -> elem.sendKeys(rushConfig.getLoginUsername()));

        Optional.of(By.xpath(rushConfig.getLoginXpathUserpwd()))
                .map(crawler::findElement)
                .ifPresent(elem -> elem.sendKeys(rushConfig.getLoginUserpwd()));

        Optional.of(By.xpath(rushConfig.getLoginXpathSubmit()))
                .map(crawler::findElement)
                .ifPresent(crawler::click);

        logger.info("login succeed");
    }

    private void fetch() throws LoginException {
        crawler.get(rushConfig.getRushUrlDelegate());

        // 重新登录
        if (crawler.getTitle().contains("登录")) {
            throw new LoginException();
        }

        List<RecordDTO> receivingRecords = Streams.of(crawler.findElements(By.xpath(rushConfig.getRushXpathReceiving())))
                .map(this::toRecordDTO)
                .collect(Collectors.toList());

        receivingRecords.stream()
                .filter(this::filter)
                .findFirst()
                .ifPresent(record->{
                    logger.info(record);
                    rushRecord(record.getId());
                });
    }

    private void rushRecord(Long id) {
        Optional.of(id)
                .map(this::getRushXpath)
                .map(By::xpath)
                .map(crawler::findElement)
                .ifPresent(crawler::clickAlert);
    }

    private String getRushXpath(long recordId) {
        String xpath = "//td[%s]/following-sibling::*//img";
        String id = String.format("contains(text(),'%d')", recordId);
        return String.format(xpath
                , id);
    }

    /**
     * 抽象方法，过滤器
     *
     * @param record
     * @return
     */
    abstract boolean filter(RecordDTO record);

    private RecordDTO toRecordDTO(WebElement element) {
        Iterator<WebElement> iterator = Optional.ofNullable(element)
                .map(e -> e.findElements(By.tagName("td")))
                .map(List::iterator)
                .orElse(Collections.emptyIterator());

        RecordDTO record = new RecordDTO();

        if (iterator.hasNext()) {
            record.setId(Long.valueOf(iterator.next().getText()));
        }
        if (iterator.hasNext()) {
            record.setRegion(iterator.next().getText());
        }
        if (iterator.hasNext()) {
            record.setTitle(iterator.next().getText());
        }
        if (iterator.hasNext()) {
            record.setLayout(iterator.next().getText());
        }
        if (iterator.hasNext()) {
            record.setFloor(NumberUtils.toInt(iterator.next().getText(), 0));
        }
        if (iterator.hasNext()) {
            record.setArea(NumberUtils.toFloat(iterator.next().getText(), 0));
        }
        if (iterator.hasNext()) {
            String priceText = iterator.next().getText();
            Matcher matcher = this.pattern.matcher(priceText);
            if (matcher.matches()) {
                String priceStr = matcher.group(1);
                record.setPrice(NumberUtils.toFloat(priceStr, 0));
            }
        }

        return record;
    }

    private void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            logger.info(e);
        }
    }
}
