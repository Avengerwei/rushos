package la.airwalkers.rush;

import la.airwalkers.config.RushConfig;
import la.airwalkers.entity.RecordDTO;
import la.airwalkers.support.webdriver.Crawler;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

/**
 * @author liupf
 */
@Profile("rent")
@Component
public class RentRusher extends Rusher {

    protected RentRusher(Crawler crawler
            , Logger logger
            , RushConfig rushConfig) {
        super(crawler, logger, rushConfig);
    }

    @Override
    boolean filter(RecordDTO record) {
        if (!StringUtils.containsAny(record.getRegion(), rushConfig.getRushRegions())) {
            return false;
        }

        if (!StringUtils.containsAny(record.getTitle(), rushConfig.getRushTitleIncludes())) {
            return false;
        }

        return true;
    }
}
